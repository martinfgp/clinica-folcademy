--PERSONA
insert into persona (idpersona,dni,nombre,apellido,telefono) values (1,'39','Julian','Dante','264567');
insert into persona (idpersona,dni,nombre,apellido,telefono) values (2,'38','Agustin','Albahaca','264654');
insert into persona (idpersona,dni,nombre,apellido,telefono) values (3,'37','Francisco','Gonzales','264679');
insert into persona (idpersona,dni,nombre,apellido,telefono) values (4,'36','Herman','Hesse','264678');

--MEDICO
insert into medico (idmedico,idpersona,profesion,consulta) values (1,3,'Odontologo',700);
insert into medico (idmedico,idpersona,profesion,consulta) values (2,4,'Diabetologo',800);


--PACIENTE
insert into paciente (idpaciente,idpersona,direccion) values (1,1,'Chimbas');
insert into paciente (idpaciente,idpersona,direccion) values (2,2,'Capital');

--TURNO
insert into turno (idturno,fecha,hora,atendido,idpaciente,idmedico) values (1,'12-12-2021','16:45:00',1,1,1);
insert into turno (idturno,fecha,hora,atendido,idpaciente,idmedico) values (2,'12-17-2021','17:45:00',0,1,1);
insert into turno (idturno,fecha,hora,atendido,idpaciente,idmedico) values (3,'12-17-2021','19:30:00',0,2,2);


--joint 3 tablas
 --select *
 --from turno as a
 --join medico as m
 --on m.idmedico=a.idmedico
 --join paciente as p  
 --on a.idpaciente=p.idpaciente;