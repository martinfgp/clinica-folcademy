package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.DTO.PacienteDTO;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;
    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    @PreAuthorize("hasAuthority('get_all_paciente')")
    @GetMapping(value = "")
    public ResponseEntity<Page<PacienteDTO>> findAll() {
        return ResponseEntity
                .ok(
                pacienteService.findAll(1,10,null));
    }

    @PreAuthorize("hasAuthority('get_all_paciente')")
    @GetMapping("/page")
    public ResponseEntity<Page<PacienteDTO>> findAll(
            @RequestParam(name="pageNumber",defaultValue = "1")Integer pageNumber,
            @RequestParam(name="pageSize",defaultValue = "2")Integer pageSize,
            @RequestParam(name="orderField",defaultValue = "domicilio")String orderField
    )
    {

        return ResponseEntity.ok(pacienteService.findAll(pageNumber,pageSize,orderField));
    }




    @PreAuthorize("hasAuthority('get_paciente')")
    @GetMapping(value = "/{idPaciente}")
    public ResponseEntity<Page<PacienteDTO>> findById(
            @PathVariable(name = "idPaciente") Integer id,
            @RequestParam(name="pageNumber",defaultValue = "1")Integer pageNumber,
            @RequestParam(name="pageSize",defaultValue = "1")Integer pageSize,
            @RequestParam(name="orderField",defaultValue = "domicilio")String orderField) {
        return ResponseEntity
                .ok()
                .body(pacienteService.findById(id,pageNumber,pageSize,orderField));
    }

    @PreAuthorize("hasAuthority('post_paciente')")
    @PostMapping("")
    public ResponseEntity<PacienteDTO> save(@RequestBody PacienteDTO dto)
    {
        return ResponseEntity.ok(pacienteService.save(dto));
    }

    @PreAuthorize("hasAuthority('put_paciente')")
    @PutMapping("/{idPaciente}")
    public ResponseEntity<PacienteDTO> edit(@PathVariable(name="idPaciente") int id,
                                                @RequestBody  PacienteDTO dto ){
        return ResponseEntity.ok(pacienteService.edit(id,dto));
    }

    @PreAuthorize("hasAuthority('delete_paciente')")
    @DeleteMapping("/{idPaciente}")
    public ResponseEntity<PacienteDTO> delete(@PathVariable(name="idPaciente") Integer id){
        return  ResponseEntity.ok(pacienteService.delete(id));
    }

}
