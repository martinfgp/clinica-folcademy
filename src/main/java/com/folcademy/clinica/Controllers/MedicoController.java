package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.DTO.MedicoDTO;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medico")
public class MedicoController {
private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {

        this.medicoService = medicoService;
    }

    @PreAuthorize("hasAuthority('get_all_medico')")
    @GetMapping("")
    public ResponseEntity<Page<MedicoDTO>> findAll()
    {
        return ResponseEntity.ok(medicoService.findAll(0,10,null));
    }

    @PreAuthorize("hasAuthority('get_all_medico')")
    @GetMapping("/page")
    public ResponseEntity<Page<MedicoDTO>> findAll(
            @RequestParam(name="pageNumber",defaultValue = "1")Integer pageNumber,
            @RequestParam(name="pageSize",defaultValue = "2")Integer pageSize,
            @RequestParam(name="orderField",defaultValue = "profesion")String orderField
            )
    {

        return ResponseEntity.ok(medicoService.findAll(pageNumber,pageSize,orderField));
    }

    @PreAuthorize("hasAuthority('get_medico')")
    @GetMapping("/{idMedico}")
    public ResponseEntity<Page<MedicoDTO>> findById(
            @PathVariable(name="idMedico") Integer id,
            @RequestParam(name="pageNumber",defaultValue = "1")Integer pageNumber,
            @RequestParam(name="pageSize",defaultValue = "1")Integer pageSize,
            @RequestParam(name="orderField",defaultValue = "profesion")String orderField)
            {
        return ResponseEntity.ok(medicoService.findById(id,pageNumber,pageSize,orderField));
    }

    @PreAuthorize("hasAuthority('post_medico')")
    @PostMapping("")
    public ResponseEntity<MedicoDTO> save(@RequestBody @Validated MedicoDTO entity)
    {
        return ResponseEntity.ok(medicoService.save(entity));
    }

    @PreAuthorize("hasAuthority('put_medico')")
    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoDTO> edit(@PathVariable(name="idMedico") int id,
                                                @RequestBody  MedicoDTO dto ){
        return ResponseEntity.ok(medicoService.edit(id,dto));
    }


    @PreAuthorize("hasAuthority('delete_medico')")
    @DeleteMapping("/{idMedico}")
    public ResponseEntity<MedicoDTO> delete(@PathVariable(name="idMedico") Integer id){
            return ResponseEntity.ok(medicoService.delete(id));
    }

}

