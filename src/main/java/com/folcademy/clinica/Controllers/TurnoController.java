package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.DTO.MedicoDTO;
import com.folcademy.clinica.Model.DTO.TurnoDTO;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turno")
public class TurnoController {

    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    @PreAuthorize("hasAuthority('get_all_turno')")
    @GetMapping(value = "")
    public ResponseEntity<Page<TurnoDTO>> findAll() {
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findAll(1,10,null))
                ;
    }

    @PreAuthorize("hasAuthority('get_all_turno')")
    @GetMapping("/page")
    public ResponseEntity<Page<TurnoDTO>> findAll(
            @RequestParam(name="pageNumber",defaultValue = "1")Integer pageNumber,
            @RequestParam(name="pageSize",defaultValue = "2")Integer pageSize,
            @RequestParam(name="orderField",defaultValue = "id")String orderField
    )
    {
        return ResponseEntity.ok(turnoService.findAll(pageNumber,pageSize,orderField));
    }


    @PreAuthorize("hasAuthority('get_turno')")
    @GetMapping(value = "/{idTurno}")
    public ResponseEntity<Page<TurnoDTO>> findById(
            @PathVariable(name = "idTurno") Integer id,
            @RequestParam(name="pageNumber",defaultValue = "1")Integer pageNumber,
            @RequestParam(name="pageSize",defaultValue = "2")Integer pageSize,
            @RequestParam(name="orderField",defaultValue = "id")String orderField) {
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findById(id,pageNumber,pageSize,orderField))
                ;
    }

    @PreAuthorize("hasAuthority('post_turno')")
    @PostMapping("")
    public ResponseEntity<TurnoDTO> save(@RequestBody TurnoDTO dto) {
        return ResponseEntity.ok(turnoService.save(dto));
    }

    @PreAuthorize("hasAuthority('put_turno')")
    @PutMapping("/{idTurno}")
    public ResponseEntity<TurnoDTO> edit(@PathVariable(name = "idTurno") int id,
                                         @RequestBody TurnoDTO dto) {
        return ResponseEntity.ok(turnoService.edit(id, dto));
    }

    @PreAuthorize("hasAuthority('delete_turno')")
    @DeleteMapping("/{idTurno}")
    public ResponseEntity<TurnoDTO> delete(@PathVariable(name = "idTurno") Integer id) {
        return ResponseEntity.ok(turnoService.delete(id));
    }


}