package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.DTO.MedicoDTO;
import com.folcademy.clinica.Model.DTO.PersonaDTO;
import com.folcademy.clinica.Services.MedicoService;
import com.folcademy.clinica.Services.PersonaService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/persona")
public class PersonaController {


    private final PersonaService personaService;

    public PersonaController(PersonaService personaService) {

        this.personaService = personaService;
    }

    @PreAuthorize("hasAuthority('get_all_persona')")
    @GetMapping("")
    public ResponseEntity<Page<PersonaDTO>> findAll()
    {
        return ResponseEntity.ok(personaService.findAll(0,10,null));
    }

    @PreAuthorize("hasAuthority('get_all_persona')")
    @GetMapping("/page")
    public ResponseEntity<Page<PersonaDTO>> findAll(
            @RequestParam(name="pageNumber",defaultValue = "1")Integer pageNumber,
            @RequestParam(name="pageSize",defaultValue = "2")Integer pageSize,
            @RequestParam(name="orderField",defaultValue = "apellido")String orderField
    )
    {
        return ResponseEntity.ok(personaService.findAll(pageNumber,pageSize,orderField));
    }
}
