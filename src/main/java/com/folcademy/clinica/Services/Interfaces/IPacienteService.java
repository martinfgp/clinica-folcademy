package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.DTO.MedicoDTO;
import com.folcademy.clinica.Model.DTO.PacienteDTO;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IPacienteService {

   // List<PacienteDTO> findAll();
    Page<PacienteDTO> findAll(Integer pageNumber, Integer pageSize, String orderField);
    Page<PacienteDTO> findById(Integer id,Integer pageNumber, Integer pageSize, String orderField);
    PacienteDTO edit(Integer idPaciente, PacienteDTO dto);
    PacienteDTO save(PacienteDTO dto);
    PacienteDTO delete(Integer idPaciente);

}
