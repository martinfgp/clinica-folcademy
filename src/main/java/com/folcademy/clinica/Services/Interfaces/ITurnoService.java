package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.DTO.MedicoDTO;
import com.folcademy.clinica.Model.DTO.TurnoDTO;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ITurnoService {
   // List<TurnoDTO> findAll() ;
    Page<TurnoDTO> findAll(Integer pageNumber, Integer pageSize, String orderField);
    Page<TurnoDTO> findById(Integer id,Integer pageNumber, Integer pageSize, String orderField);
    TurnoDTO delete(Integer id);
    TurnoDTO save(TurnoDTO dto);
    TurnoDTO edit(Integer idTurno, TurnoDTO dto);
}
