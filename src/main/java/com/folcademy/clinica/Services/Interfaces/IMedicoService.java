package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.DTO.MedicoDTO;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface IMedicoService {
       // List<MedicoDTO> findAll();
        Page<MedicoDTO> findAll(Integer pageNumber, Integer pageSize, String orderField);
        Page<MedicoDTO> findById(Integer id,Integer pageNumber, Integer pageSize, String orderField);
        MedicoDTO save(MedicoDTO dto);
        MedicoDTO edit(Integer idMedico, MedicoDTO dto);
        MedicoDTO delete(Integer idMedico);
}