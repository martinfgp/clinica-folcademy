package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.DTO.MedicoDTO;
import com.folcademy.clinica.Model.DTO.PacienteDTO;
import com.folcademy.clinica.Model.DTO.PersonaDTO;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class PacienteService implements IPacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;

    public PacienteService(PacienteRepository PacienteRepository, PacienteMapper PacienteMapper, PersonaRepository personaRepository, PersonaMapper personaMapper) {
        this.pacienteRepository = PacienteRepository;
        this.pacienteMapper = PacienteMapper;
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }


    @Override
    public Page<PacienteDTO> findAll(Integer pageNumber, Integer pageSize, String orderField) {
        if (Objects.equals(orderField, null)) {
            Pageable pageable = PageRequest.of(0, 10);
            return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDTO);

        } else {
            Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
            return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDTO);
        }
    }

    @Override
    public Page<PacienteDTO> findById(Integer id, Integer pageNumber, Integer pageSize, String orderField) {
        if (!pacienteRepository.existsById(id)) {
            throw new NotFoundException("Paciente no encontrado");
        } else {
            PacienteDTO dto = pacienteMapper.entityToDTO(pacienteRepository.findById(id).get());
            Pageable pageable= PageRequest.of(0,1);
            return pacienteRepository.findById(dto.getId(),pageable).map(pacienteMapper::entityToDTO);
        }
    }
    @Override
    public PacienteDTO edit(Integer idPaciente, PacienteDTO dto) {
        if ((!pacienteRepository.existsById(idPaciente))&&(!personaRepository.existsById(dto.getPersona().getId()))) {
            throw new NotFoundException("Paciente no encontrado");
        } else {
            dto.setId(idPaciente);
            return pacienteMapper
                    .entityToDTO(pacienteRepository
                            .save(pacienteMapper.DTOToEntity(dto)));
        }
    }

    @Override
    public PacienteDTO save(PacienteDTO dto) {
        if ((Objects.equals(dto.getDireccion(), null))&&(Objects.equals(dto.getPersona().getDni(), null)) ) {
            throw new ValidationException("Error en los datos");
        } else {
            dto.setId(null);
            dto.getPersona().setId(null);
            //guarda Persona y usando el objeto p obtengo el ID autoincremental de Persona
            Persona p=personaRepository.save(dto.getPersona());
            dto.getPersona().setId(p.getId());
            //guarda el Paciente
            return pacienteMapper.entityToDTO(pacienteRepository.save(pacienteMapper.DTOToEntity(dto)));
               }
    }
    @Override
    public PacienteDTO delete(Integer idPaciente) {
        if (!pacienteRepository.existsById(idPaciente)) {
            throw new NotFoundException("Paciente no encontrado");
        } else {
            PacienteDTO dto = pacienteMapper.entityToDTO(pacienteRepository.getById(idPaciente));
            pacienteRepository.deleteById(idPaciente);
            return dto;
        }
    }

}
