package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.DTO.MedicoDTO;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IMedicoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service("medicoService")
public class MedicoService  implements IMedicoService{
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;
    private final PersonaRepository personaRepository;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper,PersonaRepository personaRepository) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
        this.personaRepository=personaRepository;
    }

    @Override
    public Page<MedicoDTO> findAll(Integer pageNumber, Integer pageSize, String orderField) {
        if (Objects.equals(orderField, null)) {
            Pageable pageable = PageRequest.of(0, 10);
            return medicoRepository.findAll(pageable).map(medicoMapper::entityToDTO);

        } else {
            Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
            return medicoRepository.findAll(pageable).map(medicoMapper::entityToDTO);
        }
    }
    @Override
    public Page<MedicoDTO> findById(Integer id, Integer pageNumber, Integer pageSize, String orderField) {
        if (!medicoRepository.existsById(id)) {
            throw new NotFoundException("Paciente no encontrado");
        } else {
            MedicoDTO dto = medicoMapper.entityToDTO(medicoRepository.findById(id).get());
            Pageable pageable= PageRequest.of(0,1);
            return medicoRepository.findById(dto.getId(),pageable).map(medicoMapper::entityToDTO);
        }
    }
    @Override
    public MedicoDTO save( MedicoDTO dto) {
        if ((Objects.equals(dto.getProfesion(), null))&&(Objects.equals(dto.getPersona().getDni(), null)) ) {
            throw new ValidationException("Error en los datos");
        } else {
            dto.setId(null);
            dto.getPersona().setId(null);
            //guarda Persona y usando el objeto p obtengo el ID autoincremental de Persona
            Persona p=personaRepository.save(dto.getPersona());
            dto.getPersona().setId(p.getId());
            //guarda el Paciente
            return medicoMapper.entityToDTO(medicoRepository.save(medicoMapper.DTOToEntity(dto)));
        }
    }
    @Override
    public MedicoDTO edit(Integer idMedico, MedicoDTO dto) {
        if ((!medicoRepository.existsById(idMedico))&&(!personaRepository.existsById(dto.getPersona().getId()))) {
            throw new NotFoundException("Medico no encontrado");
        } else {
            dto.setId(idMedico);
            return    medicoMapper.entityToDTO(medicoRepository.save(medicoMapper.DTOToEntity(dto)));
        }
    }
    @Override
    public MedicoDTO delete(Integer idMedico) {
        if (!medicoRepository.existsById(idMedico)) {
            throw new NotFoundException("Medico no encontrado");
        } else {
            Medico entity = medicoRepository.getById(idMedico);
            medicoRepository.deleteById(idMedico);
            return medicoMapper.entityToDTO(entity);
        }
    }

}
