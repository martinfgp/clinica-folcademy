package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.DTO.PersonaDTO;
import com.folcademy.clinica.Model.DTO.TurnoDTO;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class PersonaService {

    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;

    public PersonaService(PersonaRepository personaRepository,PersonaMapper personaMapper) {
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }

    public Page<PersonaDTO> findAll(Integer pageNumber, Integer pageSize, String orderField)
    {
        if (Objects.equals(orderField, null)) { //para el GET por defecto
            Pageable pageable = PageRequest.of(0, 10);
            return personaRepository.findAll(pageable).map(personaMapper::entityToDTO);
        } else {
            Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
            return personaRepository.findAll(pageable).map(personaMapper::entityToDTO);
        }
    }


    public PersonaDTO save(PersonaDTO dto){
        if((Objects.equals(dto.getDni(), null)))
        {
            throw new ValidationException("Error en los datos");
        }else {
            dto.setId(null);

            return personaMapper.entityToDTO(personaRepository.save(personaMapper.DTOToEntity(dto)));
             }
        }


}
