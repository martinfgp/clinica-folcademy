package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.DTO.MedicoDTO;
import com.folcademy.clinica.Model.DTO.PacienteDTO;
import com.folcademy.clinica.Model.DTO.TurnoDTO;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class TurnoService implements ITurnoService {
    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository TurnoRepository, TurnoMapper TurnoMapper) {
        this.turnoRepository = TurnoRepository;
        this.turnoMapper = TurnoMapper;
    }

    @Override
    public Page<TurnoDTO> findAll(Integer pageNumber, Integer pageSize, String orderField)
    {
        if (Objects.equals(orderField, null)) { //para el GET por defecto
            Pageable pageable = PageRequest.of(0, 10);
            return turnoRepository.findAll(pageable).map(turnoMapper::entityToDTO);
        } else {
            Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
            return turnoRepository.findAll(pageable).map(turnoMapper::entityToDTO);
        }
    }
    @Override
    public Page<TurnoDTO> findById(Integer id, Integer pageNumber, Integer pageSize, String orderField) {
        if (!turnoRepository.existsById(id)) {
            throw new NotFoundException("Paciente no encontrado");
        } else {
            TurnoDTO dto = turnoMapper.entityToDTO(turnoRepository.findById(id).get());
            Pageable pageable= PageRequest.of(0,1);
            return turnoRepository.findById(dto.getId(),pageable).map(turnoMapper::entityToDTO);
        }
    }
    @Override
    public TurnoDTO edit(Integer idTurno, TurnoDTO dto) {
        if (!turnoRepository.existsById(idTurno)) {
            throw new NotFoundException("Turno no encontrado");
        } else {
            dto.setId(idTurno);
            return turnoMapper
                    .entityToDTO(
                            turnoRepository.save(
                                    turnoMapper.DTOToEntity(
                                            dto
                                    )
                            )
                    );
        }
    }
    @Override
    public TurnoDTO save(TurnoDTO dto){
        if(Objects.equals(dto.getFecha(),""))
        {
            throw new ValidationException("Error en los datos");
        }
        dto.setId(null);
        TurnoDTO saved=turnoMapper.entityToDTO(turnoRepository.save(turnoMapper.DTOToEntity(dto)));
        return saved ;
    }

    @Override
    public TurnoDTO delete(Integer idTurno) {
        if (!turnoRepository.existsById(idTurno)) {
            throw new NotFoundException("Turno no encontrado");
        } else {
            TurnoDTO dto = turnoMapper.entityToDTO(turnoRepository.getById(idTurno));
            turnoRepository.deleteById(idTurno);
            return dto;
        }
    }
}
