package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.DTO.MedicoDTO;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.DoubleStream;

@Repository
public interface MedicoRepository extends PagingAndSortingRepository<Medico,Integer> {

    Page<Medico> findAll(Pageable pageable);
    Page<Medico> findById(Integer id, Pageable pageable);
    Medico getById(Integer idMedico);
}

