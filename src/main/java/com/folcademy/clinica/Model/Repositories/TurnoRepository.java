package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.DTO.TurnoDTO;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface TurnoRepository extends PagingAndSortingRepository<Turno,Integer> {

    Page<Turno> findAll(Pageable pageable);
    Page<Turno> findById(Integer id,Pageable pageable);
    Turno getById(Integer idTurno);
}
