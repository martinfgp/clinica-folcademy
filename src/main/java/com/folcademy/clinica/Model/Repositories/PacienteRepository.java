package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.DTO.MedicoDTO;
import com.folcademy.clinica.Model.DTO.PacienteDTO;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PacienteRepository extends PagingAndSortingRepository<Paciente,Integer>{

    Page<Paciente> findAll(Pageable pageable);
    Page<Paciente> findById(Integer id,Pageable pageable);
    Paciente getById(Integer idPaciente);
}
