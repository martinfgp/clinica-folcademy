package com.folcademy.clinica.Model.Entities;

import com.folcademy.clinica.Model.DTO.MedicoDTO;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "medico")
public class Medico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
    public Integer id;
    @Column(name = "profesion", columnDefinition = "VARCHAR")
    public String profesion;
    @Column(name = "consulta", columnDefinition = "INT(10) UNSIGNED")
    public int consulta;

    @Column(name = "idpersona",columnDefinition = "INT")
    public Integer idpersona;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idpersona", referencedColumnName = "idpersona",insertable = false,updatable = false)
    public Persona persona;

}

