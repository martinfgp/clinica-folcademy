package com.folcademy.clinica.Model.Entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;


@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "persona")
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpersona", columnDefinition = "INT(10) UNSIGNED")
    public Integer id;
    @Column(name = "dni", columnDefinition = "VARCHAR")
    public String dni;
    @Column(name = "nombre", columnDefinition = "VARCHAR")
    public String nombre ;
    @Column(name = "apellido", columnDefinition = "VARCHAR")
    public String apellido;
    @Column(name = "Telefono", columnDefinition = "VARCHAR")
    public String telefono;

//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "idmedico", referencedColumnName = "idmedico",insertable = false,updatable = false)
//    public Medico medico;
//
//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "idpaciente", referencedColumnName = "idpaciente",insertable = false,updatable = false)
//    public Paciente paciente;


public Persona(Integer idPersona)
{
    this.id=idPersona;
}

}
