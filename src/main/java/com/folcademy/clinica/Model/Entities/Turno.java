package com.folcademy.clinica.Model.Entities;

import com.folcademy.clinica.Exceptions.NotFoundException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;


@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "turno")
public class Turno
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idturno", columnDefinition = "INT(10) UNSIGNED")
    public Integer id;
    @Column(name = "fecha", columnDefinition = "DATE")
    public Date fecha;
    @Column(name = "hora", columnDefinition = "TIME")
    public Time hora;
    @Column(name = "atendido", columnDefinition = "TINYINT")
    public Byte atendido;

//    @Column(name = "idpaciente",columnDefinition = "INT")
//    public Integer idpaciente;
//    @Column(name = "idmedico",columnDefinition = "INT")

//    public Integer idmedico;


    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idmedico",referencedColumnName = "idmedico",insertable = false,updatable = false)
    private Medico medico;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpaciente",referencedColumnName = "idpaciente",insertable = false,updatable = false)
    private Paciente paciente;
}