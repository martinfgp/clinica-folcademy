package com.folcademy.clinica.Model.DTO;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonaDTO {
    @NotNull
    Integer id;
    String dni;
    String nombre;
    String apellido;
    String telefono;

}
