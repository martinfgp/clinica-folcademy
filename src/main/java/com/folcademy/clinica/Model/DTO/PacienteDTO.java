package com.folcademy.clinica.Model.DTO;

import com.folcademy.clinica.Model.Entities.Persona;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PacienteDTO {
    @NotNull
    Integer id;
    String direccion;
//    Integer idpersona;
    Persona persona;
}
