package com.folcademy.clinica.Model.DTO;


import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.sun.istack.NotNull;
import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TurnoDTO {
        @NotNull
        Integer id;
         String fecha;
         String hora;
         Byte atendido;
        Medico medico;
        Paciente paciente;
//    Integer idmedico;
//    Integer idpaciente;
}
