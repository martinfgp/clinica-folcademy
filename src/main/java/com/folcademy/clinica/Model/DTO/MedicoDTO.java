package com.folcademy.clinica.Model.DTO;

import com.folcademy.clinica.Model.Entities.Persona;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicoDTO {
    @NotNull
    Integer id;
    String profesion;
    int consulta;
//    Integer idpersona;
    Persona persona;

}
