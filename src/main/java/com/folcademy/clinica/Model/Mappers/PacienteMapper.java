package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.DTO.PacienteDTO;
import com.folcademy.clinica.Model.DTO.PersonaDTO;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PacienteMapper {
    public PacienteDTO entityToDTO(Paciente entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDTO(
                                ent.getId(),
                                ent.getDireccion(),
                                ent.getPersona()
                        )
                )
                .orElse(new PacienteDTO());
    }

    public Paciente DTOToEntity(PacienteDTO dto) {
        Paciente entity = new Paciente();
        entity.setId((dto.getId()));
        entity.setDireccion(dto.getDireccion());
        entity.setIdpersona(dto.getPersona().getId());
        entity.setPersona(dto.getPersona());
        return entity;
    }



}
