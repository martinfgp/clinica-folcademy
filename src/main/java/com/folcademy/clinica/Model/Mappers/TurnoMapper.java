package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.DTO.TurnoDTO;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.sql.Time;
import java.util.Optional;

@Component
public class TurnoMapper {


    private PacienteRepository pacienteRepository;
    private MedicoRepository medicoRepository;

    public TurnoDTO entityToDTO(Turno entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDTO(
                                ent.getId(),
                                String.valueOf(ent.getFecha()), //convierte la fecha guardada como String en Date
                                String.valueOf( ent.getHora()),
                                ent.getAtendido(),
                                ent.getMedico(),
                                ent.getPaciente()
                        )
                )
                .orElse(new TurnoDTO());
    }

    public Turno DTOToEntity(TurnoDTO dto) {
        Turno entity = new Turno();
        entity.setId((dto.getId()));
        entity.setFecha(Date.valueOf(dto.getFecha()));
        entity.setHora(Time.valueOf(dto.getHora()));
        entity.setAtendido(dto.getAtendido());
        entity.setMedico(dto.getMedico());
        entity.setPaciente(dto.getPaciente());
//        entity.setIdmedico(dto.getIdmedico());
//        entity.setIdpaciente(dto.getIdpaciente());
        return entity;
    }

}
