package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.DTO.MedicoDTO;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {
    public MedicoDTO entityToDTO(Medico entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDTO(
                                ent.getId(),
                                ent.getProfesion(),
                                ent.getConsulta(),
                                ent.getPersona()
                        )
                )
                .orElse(new MedicoDTO());
    }

    public Medico DTOToEntity(MedicoDTO dto) {
        Medico entity = new Medico();
        entity.setId((dto.getId()));
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta((dto.getConsulta()));
        entity.setIdpersona(dto.getPersona().getId());
        entity.setPersona(dto.getPersona());
        return entity;
    }

}
