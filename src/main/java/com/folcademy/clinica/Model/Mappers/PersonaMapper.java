package com.folcademy.clinica.Model.Mappers;


import com.folcademy.clinica.Model.DTO.PacienteDTO;
import com.folcademy.clinica.Model.DTO.PersonaDTO;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import org.springframework.stereotype.Component;

import java.util.Optional;
@Component
public class PersonaMapper {

    public PersonaDTO entityToDTO(Persona entity)
    {
        return Optional
                .ofNullable(entity)
            .map(
                    ent -> new PersonaDTO(
                            ent.getId(),
                            ent.getDni(),
                            ent.getNombre(),
                            ent.getApellido(),
                            ent.getTelefono()
                    )
            )
            .orElse(new PersonaDTO());
    }

    public Persona DTOToEntity(PersonaDTO dto)
    {
        Persona entity = new Persona();
        entity.setId((dto.getId()));
        entity.setDni(dto.getDni());
        entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());
        entity.setTelefono(dto.getTelefono());
        return entity;
    }

}
